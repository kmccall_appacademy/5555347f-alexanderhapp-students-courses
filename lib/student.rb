require "course"

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    conflict_error if has_conflict?(course)
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    course_load_hash = Hash.new(0)
    @courses.each do |course|
      course_load_hash[course.department] += course.credits
    end
    course_load_hash
  end

  def conflict_error
    raise "scheduling conflict"
  end

  def has_conflict?(new_course)
    @courses.each do |course|
      return true if course.conflicts_with?(new_course)
    end
    false
  end
end
